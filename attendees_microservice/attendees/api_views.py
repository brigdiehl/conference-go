from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO
import json
# from events.models import Conference
# from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "created",
        "company_name",
        "conference"
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
        ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)
    # Get the Conference object and put it in the content dict
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
    )

    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.
    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information."""
    # response = []
    # attendees = Attendee.objects.all()
    # for attendee in attendees:
    #     response.append(
    #             {
    #                 "name": attendee.name,
    #                 "href": attendee.get_api_url(),
    #             }
    #     )
    # return JsonResponse({"attendees": response})


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
        try:
        # new code
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "That is not an existing Conference. "},
                status=400,
            )
    # new code
        Attendee.objects.filter(id=id).update(**content)
    # copied from get detail
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
# return JsonResponse(
    #     {
    #         "email": attendee_detail.email,
    #         "name": attendee_detail.name,
    #         "company_name": attendee_detail.company_name,
    #         "created": attendee_detail.created,
    #         "conference": {
    #             "name": attendee_detail.conference.name,
    #             "href": attendee_detail.conference.get_api_url(),
    #         }
    #         })
    # return JsonResponse({"attendee_details", attendee_detail})
